#Routing Protocols - Fall 2016 - CSE 589

The goal of this project is to create a vector routing protocol, creating 5 routers.
----------

## Getting Started

The very first step to get this system setup on your local machine would be to use the following Git command to clone the repository on your local disk. 

    git clone https://ak29@bitbucket.org/ak29/cse589-proj2.git

Alternatively, you can also download the .zip file onto your local disk and extract the files in one location. The **production** branch will contain the code that is ready for deployment. 

Once the code is downloaded you may need to check the requirements given below and update your system accordingly. 

### Prerequisites
This program has the following software requirements: 

* Linux Ubuntu
	* GCC 7.x 

### Final Points

To be Continued..


### Current Status

* Separated files for each server
* Got number of neighbours for each server