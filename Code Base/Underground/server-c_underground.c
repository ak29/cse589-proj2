#include <stdio.h>
#include <string.h>
#include <stdbool.h> // for bool datatype
#include <ctype.h>
#include <unistd.h> // for host info - display() function
#include <stdlib.h> //exit(0);
#include <arpa/inet.h>
#include <sys/socket.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <error.h>

int number_of_servers;
int number_of_neighbors;

typedef struct{
	int source_server_ID;
	int dest_server_ID;
	int distance;
}RoutingTableEntry;

typedef struct{
	char* IPAddr;
	int port_number;
}IPAddress;


RoutingTableEntry rtelist[5];
IPAddress iplist_neigh[5];

int readInitFile(char* filename){
	FILE *fp;
	char buff[255];
	fp = fopen(filename, "r+");
  	fgets(buff, 255, (FILE*)fp);
  	number_of_servers = atoi(buff); // Storing number of servers

  	fgets(buff, 255, (FILE*)fp);
  	number_of_neighbors = atoi(buff); // Storing number of neighbours
	
  	// Storing the server IP list
	for(int i=0;i<number_of_servers;i++){
		fgets(buff, 255, (FILE*)fp);
		char * pch;
		int server_id,portNumber;
	  	
	  	pch = strtok (buff," ");

	    server_id = atoi(pch);
	    server_id = server_id - 1;

	    pch = strtok (NULL, " ");
	  	
	  	iplist_neigh[server_id].IPAddr = pch;

	    pch = strtok (NULL, " ");
	  	
	    portNumber = atoi(pch);

	    iplist_neigh[server_id].port_number = portNumber;
	}

	// Storing the neighbour list

	for(int i=0;i < number_of_neighbors;i++){
		fgets(buff, 255, (FILE*) fp);

		char * pch;
	  	pch = strtok (buff," ");

	  	rtelist[i].source_server_ID = atoi(pch);

	  	pch = strtok (NULL," ");

	  	rtelist[i].dest_server_ID = atoi(pch);

	  	pch = strtok (NULL," ");
	  	rtelist[i].distance = atoi(pch);

	}

	
	
	fclose(fp);
	return 0;
}

int checkArgs(int argc, char *argv[]){
	if(argc != 5){
		printf("Invalid number of arguments. \n Run the program as ./server -t <topology-file-name> -i <routing-update-interval>\n");
		return -1;
	}
	int result_r = readInitFile(argv[2]);
	printf(" Number of servers is %d \n",number_of_servers);
	printf(" Number of neighbors is %d \n",number_of_neighbors);
	return 0;
}


int main(int argc, char *argv[]){
	if(checkArgs(argc,argv) != 0){
		return -1;
	}
	return 0;
}